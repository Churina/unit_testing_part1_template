package com.catalyte.training.monkeytesting;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MonkeyTest {
    MonkeyTrouble exercise = new MonkeyTrouble();
    @Test
    void testBothSmile(){
        assertTrue(true == exercise.monkeyTrouble(true,true));
    }

    @Test
    void testBothNotSmile(){
        assertTrue(true == exercise.monkeyTrouble(false,false));
    }

    @Test
    void testMonkey1Smile() {
        assertFalse(false != exercise.monkeyTrouble(true, false));
        // assertTrue(false == exercise.monkeyTrouble(true,false));
        //assertFalse(() -> exercise.monkeyTrouble(true, false));
    }

    @Test
    void testMonkey2Smile(){
        assertFalse(false != exercise.monkeyTrouble(false,true));
    }
}
